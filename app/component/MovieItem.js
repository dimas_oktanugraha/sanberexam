import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Button,
    TouchableOpacity,
    ActivityIndicator 
} from 'react-native';
import Axios from 'axios';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons'


export default class MovieItem extends Component {

    constructor(props) {
        super(props);
    }
f
    render() {

        let movie = this.props.movie;
        return (
            <View style={styles.container}>
                <Image source={{ uri: 'http://image.tmdb.org/t/p/original'+movie.poster_path}} style={{ height: 220, width: 150 }} />
                <View style={styles.bodyContainer}>
                    <Text numberOfLines={2} style={styles.title}>{movie.original_title}</Text>
                    <View style={styles.fav}>
                        <Icons name={'star'} size={20} style={styles. icon} />
                        <Text style={styles.score}>{movie.vote_average}/10</Text>
                    </View>
                    <Text numberOfLines={4} style={styles.overview}>{movie.overview}</Text>
                    {/* <View  style={styles.bottom}> */}
                        <Text style={styles.date}>{movie.release_date}</Text>
                        {/* <Button 
                            color="#0B50B8"
                            title="Detail"
                            onPress={()=>this.props.navigation('Detail')}/> */}
                    {/* </View> */}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: "#FFFFFF",
        marginStart: 16,
        marginEnd: 16,
        marginTop: 10
    },
    bodyContainer: {
        flex: 1,
        alignItems: 'center',
        marginStart: 16,
        marginEnd: 16,
        justifyContent: 'space-between'
    },
    title:{
        fontSize: 16,
        color: 'black',
        marginTop: 10,
        textAlign: 'center',
    },
    fav :{
        flexDirection: 'row',
    },
    score: {
        fontSize: 16,
        color: 'red',
    }, 
    icon:{
        color: "red",
        alignItems: "center",
        marginStart: 10
    },
    overview:{
        fontSize: 12,
        color: 'gray',
        textAlign: 'justify',
    },
    date: {
        fontSize: 12,
        color: 'black',
        textAlign: 'left',
        marginBottom: 10,
    },
    bottom :{
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    },
  });
