export const types = {
    NAME: 'NAME',
  }
  
  export const actionCreators = {
    name: item => {
      return { type: types.NAME, payload: item }
    },
  }
  
  const initialState = {
    name: "nama anda",
  }
  
  export const reducer = (state = initialState, action) => {
    const { name } = state
    const { type, payload } = action
  
    switch (type) {
      case types.ADD: {
        return {
          ...state,
          name: payload,
        }
      }
    }
  
    return state
  }