import { createStore } from 'redux'
import { reducer } from './UserRedux'

// Mendefinisikan store menggunakan reducer
const store = createStore(reducer)

export default store