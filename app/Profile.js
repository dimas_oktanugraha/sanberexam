import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import store from './store'

export default class Profile extends Component {
  render(){
    return (
        <View style={styles.container}>
            <View style={styles.containerTop}>
                <Icon  name={'user-o'} size={70}style={styles.imageTop} />
                {/* <Text style={styles.textName}>{store.getState().name}</Text> */}
                <Text style={styles.textName}>Dimas Oktanugraha</Text>
                <Text style={styles.textRole}>Developer</Text>
            </View>
        </View>
      );
  }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#FFFFFF"
    },
    containerTop:{
        flex: 1,
        height: 200,
        alignItems: 'center',
        backgroundColor:"#0B50B8",
        justifyContent: 'center',
        width:"100%",
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8
    },
    imageTop:{
        marginTop: 30,
        marginBottom: 5,
        color: "#ffffff"
    },
    textName:{
        fontSize: 20,
        fontWeight: "600",
        marginTop: 5,
        color: "#ffffff"
    },
    textRole:{
        fontSize: 16,
        fontWeight: "400",
        marginBottom: 30,
        color: "#ffffff"
    },
});