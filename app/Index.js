import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStore } from 'redux'
import { Provider } from 'react-redux'

import store from './store'
import Login from './Login'
import Home from './Home'
import Detail from './Detail'
import Profile from './Profile'

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

const TabsScreen = () => (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={Home} >
      </Tab.Screen>
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );

export default function AppWithStore() {
    return (
        <Provider store={store}>
          <NavigationContainer>
            <Stack.Navigator initialRouteName="Login">
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Home" component={TabsScreen} />
                <Stack.Screen name="Detail" component={Detail} />   
            </Stack.Navigator>
            </NavigationContainer>
        </Provider>
      )
    
}