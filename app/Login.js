import React, { Component } from 'react';
import { TextInput, StyleSheet, Text, View, Image, Button } from 'react-native';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { connect } from 'react-redux'
import store from './store'

import { actionCreators } from './UserRedux'

export default class Login extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            password: '',
            isError: false,
        }
    }


    loginHandler() {
        this.setState({ isError: false})
        console.log(this.state.userName, ' ', this.state.password)

        if(this.state.userName == ''){
            this.setState({ isError: true})
            return
        }

        if(this.state.password.length > 6){
            // const { dispatch } = this.props

            // dispatch(actionCreators.name(this.state.userName))
            this.props.navigation.push('Home')
        }else{
            this.setState({ isError: true})
        }
    
      }
    

    render(){
        return (
            <Provider store={store}>
                <View style={styles.container}>
                    <Image source={require('./images/logo.png')} style={styles.imageTop}/>
                    <Text style={styles.textTitle}>LOGIN</Text>

                    <View style={styles.form}>
                        <Text style={[styles.textInput, {marginTop: 30}]}>Email</Text>
                        <TextInput 
                            style = {styles.input}
                            placeholder = "Email"
                            onChangeText={userName => this.setState({ userName })}/>
                    </View>
                    
                    <View style={styles.form}>
                        <Text style={styles.textInput}>Password</Text>
                        <TextInput 
                            style = {styles.input}
                            placeholder = "Password"
                            onChangeText={password => this.setState({ password })}
                            secureTextEntry={true}/>
                    </View>

                    <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Nama Anda Kosong atau Password Kurang</Text>
                    
                    <Button 
                        color="#0B50B8"
                        title="LOGIN"
                        onPress={() => this.loginHandler()}/>
                    
                </View>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: "#FFFFFF"
    },
    imageTop: {
        width: 200,
        height: 200,
        marginTop: 20
    },
    textTitle:{
        fontSize: 20,
        fontWeight: "600",
        marginTop: 5
    },
    form: {
        alignItems:"flex-start",
        width: "70%"
    },
    textInput:{
        fontSize: 16,
        fontWeight: "600",
        marginTop: 8,
    },
    input: {
        height: 40,
        width:"100%",
        borderRadius: 6,
        paddingStart : 10,
        borderColor: '#0B50B8',
        marginBottom:20,
        borderWidth: 1,
        placeholderTextColor: "#C4C4C4",
        autoCapitalize : "none",
        underlineColorAndroid : "transparent"
     },
     errorText: {
        color: 'red',
        textAlign: 'center',
        marginBottom: 16,
    },
    hiddenErrorText: {
      color: 'transparent',
      textAlign: 'center',
      marginBottom: 16,
    },
     loginBtn:{
        width:"100%",
        color:"#0B50B8",
        borderRadius:6,
        height:40,
        alignItems:"center",
        justifyContent:"center",
        marginTop:20,
        marginBottom:10
      }
  });

