import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, FlatList, ActivityIndicator } from 'react-native';
import MovieItem from './component/MovieItem';
// import data from  './movie.json';
import Axios from 'axios';

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
          data: {},
          isLoading: true,
          isError: false
        };
      }

    componentDidMount() {
        this.getMovies()
    }

    getMovies = async () => {
        try {
          const response = await Axios.get('https://api.themoviedb.org/3/movie/now_playing?api_key=6329cec2057d748900d2549af0d9a7b5&language=en-US&page=1')
          this.setState({ isError: false, isLoading: false, data: response.data })
        } catch (error) {
          this.setState({ isLoading: false, isError: true })
        }
      }

    render(){
        //  If load data
        if (this.state.isLoading) {
            return (
            <View
                style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                <ActivityIndicator size='large' color='red' />
            </View>
            )
        }
        // If data not fetch
        else if (this.state.isError) {
            return (
            <View
                style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
                <Text>Terjadi Error Saat Memuat Data</Text>
            </View>
            )
        }

        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <Text style={styles.title}>Now Playing</Text>
                    <FlatList
                    data={this.state.data.results}
                    renderItem={(movie)=><MovieItem movie={movie.item} />}
                    keyExtractor={(item)=>item.id}
                    ItemSeparatorComponent={()=><View style={{height:0.5, backgroundColor:'#E5E5E5'}}/>}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    body: {
        flex: 1,
    },
    title:{
        fontSize: 20,
        color: 'red',
        marginTop: 10,
        marginBottom: 10,
        textAlign: 'center',
    },
});
